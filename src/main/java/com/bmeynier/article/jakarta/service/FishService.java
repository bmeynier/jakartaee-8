package com.bmeynier.article.jakarta.service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public interface FishService {

    int countByFamily(String fishFamily);

    float buy(String shopName, String fishName, int quantity);

    float sell(String shopName, String fishName, int quantity);

    CompletableFuture<Void> callManager(Executor executor);
}
