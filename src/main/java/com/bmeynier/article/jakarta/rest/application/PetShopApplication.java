package com.bmeynier.article.jakarta.rest.application;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/petshop")
public class PetShopApplication extends Application {}
